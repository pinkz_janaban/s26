- What directive is used by Node.js in loading the modules it needs?
Answer: 'requre'
- What Node.js module contains a method for server creation?
Answer: HTTP Built-in Node.js
- What is the method of the http object responsible for creating a server using Node.js?
Answer: http.createServer() method
- What method of the response object allows us to set status codes and content types?
Answer: .writeHead()
- Where will console.log() output its contents when run in Node.js?
Answer: web browser
- What property of the request object contains the address's endpoint?
Answer: route